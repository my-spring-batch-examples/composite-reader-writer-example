package com.example.job;

import java.util.Arrays;
import java.util.List;
import javax.sql.DataSource;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.builder.MultiResourceItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

@Configuration
@EnableBatchProcessing
@AllArgsConstructor
public class ExampleJobConfig {
  private JobBuilderFactory jobBuilderFactory;
  private StepBuilderFactory stepBuilderFactory;
  private ItemProcessor<Fruit, Fruit> fruitProcessor;
  private DataSource dataSource;

  @Bean
  public Job exampleJob(Step exampleStep) {
    return jobBuilderFactory
        .get("exampleJob")
        .incrementer(new RunIdIncrementer())
        .start(exampleStep)
        .build();
  }

  @Bean
  public Step exampleStep() {
    return stepBuilderFactory
        .get("exampleStep")
        .<Fruit, Fruit>chunk(1)
        .reader(multiResourceItemReader())
        .processor(fruitProcessor)
        .writer(compositeItemWriter())
        .build();
  }

  @Bean
  @StepScope
  public MultiResourceItemReader<Fruit> multiResourceItemReader() {
    List<FileSystemResource> resourceList =
        Arrays.asList(new FileSystemResource("input1.csv"), new FileSystemResource("input1.csv"));

    return new MultiResourceItemReaderBuilder<Fruit>()
        .name("multiResourceItemReader")
        .resources(resourceList.toArray(new FileSystemResource[0]))
        .delegate(fruitReader())
        .build();
  }

  @Bean
  public FlatFileItemReader<Fruit> fruitReader() {
    return new FlatFileItemReaderBuilder<Fruit>()
        .name("fruitReader")
        .delimited()
        .names(new String[] {"name", "price"})
        .fieldSetMapper(
            new BeanWrapperFieldSetMapper<Fruit>() {
              {
                setTargetType(Fruit.class);
              }
            })
        .build();
  }

  @Bean
  @StepScope
  public CompositeItemWriter<Fruit> compositeItemWriter() {
    CompositeItemWriter<Fruit> writer = new CompositeItemWriter<Fruit>();
    writer.setDelegates(Arrays.asList(fruitCsvWriter(), fruitTableWriter(dataSource)));
    return writer;
  }

  @Bean
  public FlatFileItemWriter<Fruit> fruitCsvWriter() {
    return new FlatFileItemWriterBuilder<Fruit>()
        .name("fruitCsvWriter")
        .resource(new FileSystemResource("output.csv"))
        .delimited()
        .names(new String[] {"name", "price"})
        .build();
  }

  @Bean
  public JdbcBatchItemWriter<Fruit> fruitTableWriter(DataSource dataSource) {
    return new JdbcBatchItemWriterBuilder<Fruit>()
        .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
        .sql("insert into fruits(fruit_name, price) values(:name, :price)")
        .dataSource(dataSource)
        .build();
  }
}
