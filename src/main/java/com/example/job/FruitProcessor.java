package com.example.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FruitProcessor implements ItemProcessor<Fruit, Fruit> {
  @Override
  public Fruit process(Fruit item) throws Exception {
    log.info("FruitProcessor: " + item.toString());
    Fruit result = new Fruit();
    result.setName(item.getName());
    result.setPrice(item.getPrice() * 10);
    return result;
  }
}
