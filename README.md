# composite-reader-writer-example

## RUN

1. `docker-compose up -d`
1. `./gradlew bootRun`

First, this batch reads `input1.csv` and `input2.csv` by `MultiResourceItemReader`.  
Then, this batch writes to `output.csv` and `fruits` table by `CompositeItemWriter`.
