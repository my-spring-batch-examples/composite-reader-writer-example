create table fruits(
    id serial primary key,
    fruit_name varchar(100) not null,
    price int not null,
    created_date timestamp NOT NULL default now(),
    updated_date timestamp NOT NULL default now()
);
insert into fruits(fruit_name, price)
values('melon', 6000);